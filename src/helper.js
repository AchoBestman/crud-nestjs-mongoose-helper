"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.errorFilter = exports.arrayToString = exports.limitData = void 0;
const limitData = (limit) => {
    return limit !== null && limit !== void 0 ? limit : 100;
};
exports.limitData = limitData;
const arrayToString = (data) => {
    let convert = '';
    if (data && Array.isArray(data)) {
        data.forEach(el => {
            convert += el;
        });
    }
    else {
        convert = `${data}`;
    }
    return convert;
};
exports.arrayToString = arrayToString;
const errorFilter = (data) => {
    let errorMessage = {};
    if (data.errors) {
        for (var err in data.errors) {
            if (err) {
                errorMessage[err] = data.errors[err].message
                    .split(' (type')[0].replace('"', '')
                    .replace('"', '').replace("\\", '').replace('\\', '');
            }
        }
    }
    if (!data.errors && (data.name == 'ValidationError' || data.name == 'CastError')) {
        errorMessage[data.path] = data.message.split(' (type')[0]
            .replace('"', '').replace('"', '').replace("\\", '').replace('\\"', '');
    }
    return errorMessage;
};
exports.errorFilter = errorFilter;
//# sourceMappingURL=helper.js.map