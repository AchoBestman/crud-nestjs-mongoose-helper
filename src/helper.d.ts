export declare const limitData: (limit: number | undefined) => number;
export declare const arrayToString: (data: string[] | string | undefined) => string;
export declare const errorFilter: (data: any) => {};
