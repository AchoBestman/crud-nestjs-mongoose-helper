export const limitData = (limit: number|undefined)=>{

  return limit ?? 100;
}

export const arrayToString = (data: string[]|string|undefined)=> {

  let convert = '';
  if(data && Array.isArray(data)){
      data.forEach(el =>{
          convert += el;
      })
  }
  else{
    convert = `${data}`;
  }

  return convert;
}

export const errorFilter = (data: any)=>{

  let errorMessage = {};
  if (data.errors) {
      for(var err in data.errors){
          if(err){
              errorMessage[err] = data.errors[err].message
              .split(' (type')[0].replace('"', '')
              .replace('"', '').replace("\\", '').replace('\\', '');
          }
      }

  }if(!data.errors && (data.name == 'ValidationError' || data.name == 'CastError')){
      errorMessage[data.path] = data.message.split(' (type')[0]
      .replace('"', '').replace('"', '').replace("\\", '').replace('\\"', '');
  }

  return errorMessage;
}