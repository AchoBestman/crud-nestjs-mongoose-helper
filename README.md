<p align="center">
  <a href="https://gitlab.com/AchoBestman/crud-nestjs-mongoose-helper" target="blank">crud-nestjs-mongoose-helper</a>
</p>

## Description

[crud-nestjs-mongoose-helper](https://gitlab.com/AchoBestman/crud-nestjs-mongoose-helper) a microservice to help nodejs/nestjs mongodb developers to speed up the creation of their crud with a minimum of code.

## Installation

```bash
$ npm install
```

## Use

import { exist, create, createIfne, one, all, allDistinct, put, destroy } from "crud-nestjs-mongoose-helper/index"
# verify if model exist
const data = await exist(databaseTableModel, {field: value});

# create a model
const data = await create(databaseTableModel, body, 'onePopulateTable', fieldsToTakeInThePopulateTable);

# create a model if not exist
const data = await createIfne(databaseTableModel, body, 'onePopulateTable', fieldsToTakeInThePopulateTable);

# find a model
const data = await one(databaseTableModel, filterParams, fieldsToTakeInTheTable, 'onePopulateTable', fieldsToTakeInThePopulateTable);

# find many models
const data = await all(databaseTableModel, filterParams, fieldsToTakeInTheTable, sortParams, limitLength 'onePopulateTable', fieldsToTakeInThePopulateTable);

# find distinct models
const data = await allDistinct(databaseTableModel, 'onePopulateTable', filterParams);

#update a model
const data = await put(databaseTableModel, body, filterParams, 'onePopulateTable', fieldsToTakeInThePopulateTable);

# delete a model
const data = await destroy(databaseTableModel, filterParams);
## Support

crud-nestjs-mongoose-helper is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join me, please [read more here](https://www.linkedin.com/in/achile-aikpe-018654196/).

## Stay in touch

- Author - [Achile Aïkpé](https://www.linkedin.com/in/achile-aikpe-018654196/)
- LinkedIn - [ACHILE AIKPE](https://www.linkedin.com/in/achile-aikpe-018654196/)

## License

crud-nestjs-mongoose-helper is [MIT licensed](LICENSE).
