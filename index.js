"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.destroy = exports.put = exports.allDistinct = exports.all = exports.one = exports.createIfne = exports.create = exports.exist = void 0;
const common_1 = require("@nestjs/common");
const error_1 = require("./src/error");
const helper_1 = require("./src/helper");
const exist = async (model, filter) => {
    const data = await model.findOne(filter).exec().catch(err => {
        return error_1.error;
    });
    if (!data) {
        return false;
    }
    return data;
};
exports.exist = exist;
const create = async (model, body, populate, populateFields) => {
    let data = await new model(body).save().catch(err => {
        throw (0, error_1.error)(err, common_1.HttpStatus.INTERNAL_SERVER_ERROR);
    });
    if (populate) {
        data = data.populate(populate, (0, helper_1.arrayToString)(populateFields));
    }
    return data;
};
exports.create = create;
const createIfne = async (model, body, filter, populate, populateFields) => {
    const one = await (0, exports.exist)(model, filter);
    if (one) {
        return { message: "data already exist !", body: one };
    }
    let data = await new model(body).save().catch(err => {
        throw (0, error_1.error)(err, common_1.HttpStatus.INTERNAL_SERVER_ERROR);
    });
    if (populate) {
        data = data.populate(populate, (0, helper_1.arrayToString)(populateFields));
    }
    return data;
};
exports.createIfne = createIfne;
const one = async (model, filter, fields, populate, populateFields) => {
    let data = await model.findOne(filter, (0, helper_1.arrayToString)(fields)).catch(err => {
        throw (0, error_1.error)(err, common_1.HttpStatus.NOT_FOUND);
    });
    if (populate) {
        data = data.populate(populate, (0, helper_1.arrayToString)(populateFields));
    }
    return data;
};
exports.one = one;
const all = async (model, filter, fields, sort, limit, populate, populateFields) => {
    const data = await model.find(filter, (0, helper_1.arrayToString)(fields))
        .populate(populate || '', (0, helper_1.arrayToString)(populateFields)).sort(sort).limit((0, helper_1.limitData)(limit)).catch(err => {
        throw (0, error_1.error)(err, common_1.HttpStatus.NOT_FOUND);
    });
    return data;
};
exports.all = all;
const allDistinct = async (model, field, filter, sort, limit, populate, populateFields) => {
    const data = await model.distinct(field, filter)
        .populate(populate || '', (0, helper_1.arrayToString)(populateFields)).sort(sort).catch(err => {
        throw (0, error_1.error)(err, common_1.HttpStatus.NOT_FOUND);
    });
    return data;
};
exports.allDistinct = allDistinct;
const put = async (model, body, filter, populate, populateFields) => {
    const data = await model.findOneAndUpdate(filter, body).catch(err => {
        throw (0, error_1.error)(err, common_1.HttpStatus.NOT_FOUND);
    });
    if (!data) {
        return (0, error_1.error)('Model not found', common_1.HttpStatus.NOT_FOUND);
    }
    return await (0, exports.one)(model, { _id: data._id }, undefined, populate, (0, helper_1.arrayToString)(populateFields));
};
exports.put = put;
const destroy = async (model, filter) => {
    await model.findByIdAndDelete(filter).catch(err => {
        throw (0, error_1.error)(err, common_1.HttpStatus.NOT_FOUND);
    });
    return 1;
};
exports.destroy = destroy;
//# sourceMappingURL=index.js.map